package com.example.pigemail.firebase

import com.google.firebase.auth.FirebaseAuth
import dagger.Component

@Component(modules = [FirebaseModule::class])
interface IFirebaseComponent {

    fun getFirebaseHelper(): FirebaseHelper


}
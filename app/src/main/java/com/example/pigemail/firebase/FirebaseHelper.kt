package com.example.pigemail.firebase

import android.os.Bundle
import androidx.navigation.fragment.findNavController
import com.example.pigemail.R
import com.example.pigemail.ui.utilis.showToast
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import java.security.AuthProvider
import java.security.Provider
import javax.inject.Inject


class FirebaseHelper {

    var auth:FirebaseAuth = FirebaseAuth.getInstance()
    var database:FirebaseDatabase = FirebaseDatabase.getInstance()

    fun userIsAutorization():Boolean{
        return auth.currentUser != null
    }

    fun signOut() {
        auth.signOut()
    }

    fun getRoot(): DatabaseReference {
        return database.reference
    }

    }
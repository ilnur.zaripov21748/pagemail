package com.example.pigemail.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.pigemail.R
import com.example.pigemail.databinding.FragmentEnterPhoneNumberBinding
import com.example.pigemail.ui.fragments.presenter.DaggerIPresenterComponent
import com.example.pigemail.ui.fragments.presenter.EnterPhoneNumberPresenter
import com.example.pigemail.ui.fragments.presenter.FragmentModule
import com.example.pigemail.ui.fragments.presenter.IPresenterComponent
import javax.inject.Inject

class EnterPhoneNumberFragment : BaseFragment(R.layout.fragment_enter_phone_number) {

    private lateinit var binding: FragmentEnterPhoneNumberBinding
    private lateinit var numberPhone: String


    @Inject
    lateinit var enterPhoneNumberPresenter: EnterPhoneNumberPresenter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var a:IPresenterComponent = DaggerIPresenterComponent.builder()
            .fragmentModule(FragmentModule(this))
            .build()
        enterPhoneNumberPresenter = a.getPresenter()
        binding = FragmentEnterPhoneNumberBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.registerGoEnterCode.setOnClickListener {
            numberPhone = binding.inputPhoneNumber.text.toString()
            enterPhoneNumberPresenter.sendPhoneNumberIsCorrect(numberPhone)
        }
    }

}
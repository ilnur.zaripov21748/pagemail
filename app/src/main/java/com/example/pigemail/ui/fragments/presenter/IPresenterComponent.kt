package com.example.pigemail.ui.fragments.presenter

import dagger.Component

@Component(modules = [PresenterModule::class])
interface IPresenterComponent {

    fun getPresenter():EnterPhoneNumberPresenter
}
package com.example.pigemail.ui.utilis

import android.text.Editable
import android.text.TextWatcher
import io.reactivex.internal.operators.single.SingleDoOnSuccess

class AppTextWatcher(val onSuccess: (Editable?) -> Unit): TextWatcher {
    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    override fun afterTextChanged(s: Editable?) {
        onSuccess(s)
    }
}
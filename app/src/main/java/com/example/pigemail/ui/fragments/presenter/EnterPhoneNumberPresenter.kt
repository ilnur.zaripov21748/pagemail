package com.example.pigemail.ui.fragments.presenter

import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment.findNavController
import com.example.pigemail.R
import com.example.pigemail.firebase.DaggerIFirebaseComponent
import com.example.pigemail.firebase.FirebaseHelper
import com.example.pigemail.firebase.IFirebaseComponent
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class EnterPhoneNumberPresenter(var fragment: Fragment) {

    @Inject
    lateinit var firebaseHelper: FirebaseHelper



    fun sendPhoneNumberIsCorrect(numberPhone: String): Boolean {
        if (numberPhone.isNotEmpty()) {
//            Авторизвация номера пользователя
//            authUser()
            authUser(numberPhone)
            return true
        } else {
//            Строка пуста
//            showToast("Введите корректный номер телефона")
            return false
        }
    }

    private fun authUser(numberPhone: String) {
        fragment?.let {
            PhoneAuthProvider.verifyPhoneNumber(
                PhoneAuthOptions
                    .newBuilder(FirebaseAuth.getInstance())
                    .setActivity(fragment.requireActivity())
                    .setPhoneNumber(numberPhone)
                    .setTimeout(30, TimeUnit.SECONDS)
                    .setCallbacks(setCallback(numberPhone))
                    .build()
            )
        }
    }

    private fun setCallback(numberPhone: String): PhoneAuthProvider.OnVerificationStateChangedCallbacks {
    return object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        //Ферификация успешна
        override fun onVerificationCompleted(p0: PhoneAuthCredential) {
            firebaseHelper.auth.signInWithCredential(p0).addOnCompleteListener {
                if (it.isSuccessful) {
                    findNavController(fragment).navigate(R.id.chatsFragment)
                } else Toast.makeText(fragment.context,it.exception?.message.toString(),Toast.LENGTH_SHORT)
            }
        }


        //Ошибака ферификации
        override fun onVerificationFailed(exception: FirebaseException) {
            Toast.makeText(fragment.context,exception.message.toString(),Toast.LENGTH_SHORT)
        }

        //СМС отправлено
        override fun onCodeSent(id: String, token: PhoneAuthProvider.ForceResendingToken) {
            val arg = Bundle()
            arg.putString("phone_number", numberPhone)
            arg.putString("id", id)
            findNavController(fragment).navigate(R.id.enterCodeFragment, arg)

        }
    }
    }


}
package com.example.pigemail.ui.fragments.presenter

import androidx.fragment.app.Fragment
import dagger.Module
import dagger.Provides

@Module(includes = [FragmentModule::class])
class PresenterModule {

    @Provides
    fun getEnterPhoneNumberPresenter(fragment : Fragment):EnterPhoneNumberPresenter{
        return EnterPhoneNumberPresenter(fragment)
    }
}
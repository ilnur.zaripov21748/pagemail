package com.example.pigemail.ui.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.pigemail.R
import com.example.pigemail.databinding.FragmentEnterCodeBinding
import com.example.pigemail.firebase.DaggerIFirebaseComponent
import com.example.pigemail.firebase.FirebaseHelper
import com.example.pigemail.firebase.IFirebaseComponent
import com.example.pigemail.ui.utilis.AppTextWatcher
import com.example.pigemail.ui.utilis.showToast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class EnterCodeFragment : Fragment(R.layout.fragment_enter_code) {

    private lateinit var binding:FragmentEnterCodeBinding
    private lateinit var mPhoneNumber: String
    private lateinit var id: String

    @Inject
    lateinit var firebaseHelper: FirebaseHelper
    lateinit var component:IFirebaseComponent

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        component = DaggerIFirebaseComponent.create()
        firebaseHelper = component.getFirebaseHelper()

        binding = FragmentEnterCodeBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mPhoneNumber = requireArguments().getString("phone_number")!!
        id = requireArguments().getString("id")!!
        activity?.title = mPhoneNumber
        binding.inputCode.addTextChangedListener(AppTextWatcher{
               val code:String = binding.inputCode.text.toString()
                if (code.length == 6){
                    verificationCode()
                }

        })
    }

    private fun verificationCode() {
        val code:String = binding.inputCode.text.toString()
        val credential:PhoneAuthCredential = PhoneAuthProvider.getCredential(id,code)
        firebaseHelper.auth.signInWithCredential(credential).addOnCompleteListener{
            if (it.isSuccessful){
                showToast("Добро Пожаловать")
                findNavController().navigate(R.id.chatsFragment)


            }
                else showToast(it.exception?.message.toString())

        }
    }


}
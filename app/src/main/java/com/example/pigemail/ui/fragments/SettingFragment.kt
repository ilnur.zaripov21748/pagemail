package com.example.pigemail.ui.fragments

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.navigation.fragment.findNavController
import com.example.pigemail.R
import com.example.pigemail.firebase.DaggerIFirebaseComponent
import com.example.pigemail.firebase.FirebaseHelper
import com.example.pigemail.firebase.IFirebaseComponent
import javax.inject.Inject

class SettingFragment : BaseFragment(R.layout.fragment_setting) {

    @Inject
    lateinit var firebaseHelper: FirebaseHelper
    lateinit var component: IFirebaseComponent

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        component = DaggerIFirebaseComponent.create()
        firebaseHelper = component.getFirebaseHelper()
    }

    override fun onResume() {
        super.onResume()
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        activity?.menuInflater?.inflate(R.menu.setting_action_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.settingsMenuExit -> {
                firebaseHelper.signOut()
                findNavController().navigate(R.id.enterPhoneNumberFragment)
            }

        }
        return true
    }
}
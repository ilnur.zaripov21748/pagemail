package com.example.pigemail.ui.fragments.presenter

import androidx.fragment.app.Fragment
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class FragmentModule (var fragment: Fragment) {
    @Provides
    fun fragment(): Fragment {
        return fragment
    }
}
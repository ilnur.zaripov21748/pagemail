package com.example.pigemail

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import com.example.pigemail.databinding.ActivityMainBinding
import com.example.pigemail.firebase.DaggerIFirebaseComponent
import com.example.pigemail.firebase.FirebaseHelper
import com.example.pigemail.firebase.IFirebaseComponent
import com.example.pigemail.ui.objects.AppDrawer
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityMainBinding
    private lateinit var mAppDrawer: AppDrawer
    private lateinit var mToolbar: Toolbar

    lateinit var component: IFirebaseComponent

    @Inject
    lateinit var firebaseHelper: FirebaseHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityMainBinding.inflate(layoutInflater)

        component = DaggerIFirebaseComponent.create()
        firebaseHelper = component.getFirebaseHelper()




        setContentView(mBinding.root)
    }

    override fun onStart() {
        super.onStart()
        mToolbar = mBinding.mainToolbar
                setSupportActionBar(mToolbar)
        mAppDrawer = AppDrawer(this, mToolbar)
        mAppDrawer.create()
        findNavController(R.id.nav_host_fragment)
            .addOnDestinationChangedListener { controller, destination, arguments ->
                when (destination.id){
                    R.id.enterPhoneNumberFragment ->
                        mAppDrawer.hide()
                    R.id.chatsFragment ->
                        mAppDrawer.show()
                }
            }

//      Проверка на Авторизованность пользователя
        if (firebaseHelper.userIsAutorization()) {
            //mAppDrawer.create()
        } else {
            navigate(R.id.enterPhoneNumberFragment)
            title = getString(R.string.register_title)
        }

    }

    private fun navigate(layout: Int) {
        findNavController(R.id.nav_host_fragment).navigate(layout)
    }
}